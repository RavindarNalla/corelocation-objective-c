//
//  Structures.h
//  Alerts Map App
//
//  Created by Manikantesh on 27/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Structures : NSObject

@end

#pragma mark - LocationStruct
@interface LocationStruct : NSObject<NSCoding>
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) NSString *locationDetailedAddress;
@property (nonatomic, strong) NSString *locationTitle;
@end



NS_ASSUME_NONNULL_END
