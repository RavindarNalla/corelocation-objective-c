//
//  Structures.m
//  Alerts Map App
//
//  Created by Manikantesh on 27/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import "Structures.h"

@implementation Structures
@end


@implementation LocationStruct

- (id)initWithCoder:(NSCoder *)decoder {
    
    if (self = [super init]) {
        self.latitude = [decoder decodeDoubleForKey:@"latitude"];
        self.longitude = [decoder decodeDoubleForKey:@"longitude"];
        self.locationDetailedAddress = [decoder decodeObjectForKey:@"locationDetailedAddress"];
        self.locationTitle = [decoder decodeObjectForKey:@"locationTitle"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeDouble:self.latitude forKey:@"latitude"];
    [encoder encodeDouble:self.longitude forKey:@"longitude"];
    [encoder encodeObject:self.locationDetailedAddress forKey:@"locationDetailedAddress"];
    [encoder encodeObject:self.locationTitle forKey:@"locationTitle"];
}


@end
