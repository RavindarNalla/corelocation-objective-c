//
//  AppDelegate.h
//  Alerts Map App
//
//  Created by Manikantesh on 25/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

