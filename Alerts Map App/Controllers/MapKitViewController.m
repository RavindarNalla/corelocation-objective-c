//
//  MapKitViewController.m
//  Alerts Map App
//
//  Created by Manikantesh on 27/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import "MapKitViewController.h"



@interface MapKitViewController ()<MKMapViewDelegate,UISearchBarDelegate>{
    MKPointAnnotation *annot;
    double latitide;
    double longitude;
    NSString *locationDetailedAddress;
    NSString *locationTitle;
    MKMapCamera *camera;
    BOOL isSavedLocationsPinned;
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *geofences;
@property (nonatomic, strong) MKAnnotationView *mkAnnotationView;

@end

@implementation MapKitViewController

static NSString *GeofenceCellIdentifier = @"GeoLocationsCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _mkAnnotationView = [MKAnnotationView new];
    _mkAnnotationView.tag = 0;
    [self showMarkedFavouriteLocations];
    [self setupView];
    
    
    self.mapKitView.showsUserLocation = YES;
//    [self.mapKitView.userLocation removeObserver:self forKeyPath:@"location"];
    self.mapKitView.showsTraffic = NO;
    self.mapKitView.showsPointsOfInterest = YES;
    self.mapKitView.mapType = MKMapTypeHybrid;
    self.mapKitView.delegate = self;
    
    self.searchBar.delegate = self;
    self.tableView.hidden = YES;
    
    NSMutableArray *geofencesArray = [NSMutableArray new];
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedArray = [currentDefaults objectForKey:@"Geofences"];
    if (savedArray != nil)
    {
        NSArray *oldArray = [NSKeyedUnarchiver unarchiveObjectWithData:savedArray];
        if (oldArray != nil) {
            geofencesArray = [[NSMutableArray alloc] initWithArray:oldArray];
        } else {
            geofencesArray = [[NSMutableArray alloc] init];
        }
    }
    
    
    if([self.typeStr isEqualToString:@"NEW"]){
        UIAlertController* alertView = [UIAlertController alertControllerWithTitle:@"Message"
                                                                           message:@"Please hold on your finger for a while on a location to pin that location"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                   }];
        [alertView addAction:ok];
        [self  presentViewController:alertView animated:YES completion:nil];
    }
    
    
}

-(void)showMarkedFavouriteLocations{
    
    
    NSMutableArray *favouriteLocationsArray = [NSMutableArray new];
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedArray = [currentDefaults objectForKey:@"Geofences"];
    if (savedArray != nil)
    {
        NSArray *oldArray = [NSKeyedUnarchiver unarchiveObjectWithData:savedArray];
        if (oldArray != nil) {
            favouriteLocationsArray = [[NSMutableArray alloc] initWithArray:oldArray];
        } else {
            favouriteLocationsArray = [[NSMutableArray alloc] init];
        }
    }
    
    if ((favouriteLocationsArray.count > 0)){
        for (int i=0; i<[favouriteLocationsArray count]; i++) {
            
            //Retrieve the NSDictionary object in each index of the array.
            LocationStruct *locationObj = [favouriteLocationsArray objectAtIndex:i];
            
            MKPointAnnotation *savedLocationAnnotation = [[MKPointAnnotation alloc] init];
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = locationObj.latitude;
            coordinate.longitude = locationObj.longitude;
            savedLocationAnnotation.coordinate = coordinate;
            savedLocationAnnotation.title = locationObj.locationTitle;
            [self.mapKitView addAnnotation:savedLocationAnnotation];
        }
    }
    
    //isSavedLocationsPinned = true;
    _mkAnnotationView.tag = 1;
    NSLog(@"mkAnnotationViewTag: %ld",(long)_mkAnnotationView.tag);
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context

{
    if (self.tableView.contentSize.height < 300) {
        self.tableView.frame = CGRectMake(self.searchBar.frame.origin.x, self.searchBar.frame.origin.y+44, self.tableView.frame.size.width,self.tableView.contentSize.height);
        
        //            CGRect frame = self.tableView.frame;
        //            frame.size = self.tableView.contentSize;
        //            self.tableView.frame = frame;
        
    }
    
}



- (void)setupView {
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Create Add Button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveLocation:)];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Save", nil)];
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.1; //user needs to press for 2 seconds
    [self.mapKitView addGestureRecognizer:lpgr];
}

- (void)saveLocation:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self->latitide && self->longitude && self->locationDetailedAddress){
            
            LocationStruct *objStruct = [[LocationStruct alloc] init];
            objStruct.latitude = self->latitide;
            objStruct.longitude = self->longitude;
            objStruct.locationDetailedAddress = self->locationDetailedAddress;
            objStruct.locationTitle = self->locationTitle;
            
            [self.delegate saveLocationDelegate:objStruct];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertController* alertView = [UIAlertController alertControllerWithTitle:@"Warning!"
                                                                               message:@"Please Pin a Location to Save"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                       }];
            [alertView addAction:ok];
            [self  presentViewController:alertView animated:YES completion:nil];
        }
    });
    
    
}

#pragma mark - MKMapView Delegate Methods

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(aUserLocation.coordinate, 800, 800);
    
    //MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
//    [aMapView setRegion:region animated:YES];
//    [self.mapKitView setRegion:[self.mapKitView regionThatFits:region] animated:YES];
}

/* Position the camera after updating the maps */
-(void) positionCamera : (CLLocationCoordinate2D )coord{
    camera = [MKMapCamera cameraLookingAtCenterCoordinate:coord fromEyeCoordinate:coord eyeAltitude:10000000];
    self.mapKitView.camera = camera;
}


/* Handle Click on the annotation */
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    NSLog(@"Clicked on Accessory View");
}

/* If you implement this, you will not see the annotation view */
- (void)mapView:(MKMapView *)mapView1 didSelectAnnotationView:(MKAnnotationView *)view
{
    [self.mapKitView deselectAnnotation:view.annotation animated:YES];
    NSLog(@"didSelectAnnotationView");
}

/* This function renders the Overlay if it is a polyline or Circle */
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if([overlay isKindOfClass:[MKCircle class]]){
        MKCircleRenderer *circleView = [[MKCircleRenderer alloc] initWithOverlay:overlay];
        circleView.strokeColor = [UIColor blueColor];
        circleView.fillColor = [[UIColor blueColor] colorWithAlphaComponent:0.4];
        circleView.lineWidth = 1;
        return circleView;
    }
    
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 5.0;
    
    return renderer;
}

//- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
//{
//    static NSString *SFAnnotationIdentifier = @"SFAnnotationIdentifier";
//    MKPinAnnotationView *pinView =
//    (MKPinAnnotationView *)[self.mapKitView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
//    if (!pinView)
//    {
//        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
//                                                                         reuseIdentifier:SFAnnotationIdentifier] ;
//        UIImage *flagImage = [UIImage imageNamed:@"pin"];
//        // You may need to resize the image here.
//        annotationView.image = flagImage;
//        return annotationView;
//    }
//    else
//    {
//        pinView.annotation = annotation;
//    }
//    return pinView;
//}
//-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotationPoint
//{
//    if (!isSavedLocationsPinned){
//        NSLog(@"_mkAnnotationView.tag: %ld",(long)_mkAnnotationView.tag);
//        isSavedLocationsPinned = true;
//        return  nil;
//    }
//    else {
//        if (annotationPoint == mapView.userLocation) return nil;
//
//        if ([annotationPoint isKindOfClass:[MKUserLocation class]])//keep the user as default
//            return nil;
//
//        static NSString *annotationIdentifier = @"annotationIdentifier";
//        MKPinAnnotationView *pinView = [[MKPinAnnotationView alloc]initWithAnnotation:annotationPoint reuseIdentifier:annotationIdentifier];
//        pinView.pinTintColor = [UIColor greenColor];
//        pinView.canShowCallout = YES;
//        //now we can throw an image in there
//
//        return pinView;
//    }
//}

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    [self.mapKitView removeAnnotation:annot];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapKitView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapKitView convertPoint:touchPoint toCoordinateFromView:self.mapKitView];
    
    latitide = touchMapCoordinate.latitude;
    longitude = touchMapCoordinate.longitude;
    
    [self getLocationDetails:latitide long:longitude];
    
    annot = [[MKPointAnnotation alloc] init];
    annot.coordinate = touchMapCoordinate;
    [self.mapKitView addAnnotation:annot];

    MKMapRect r = [self.mapKitView visibleMapRect];
    MKMapPoint pt = MKMapPointForCoordinate([annot coordinate]);
    r.origin.x = pt.x - r.size.width * 0.5;
    r.origin.y = pt.y - r.size.height * 0.5;
    [self.mapKitView setVisibleMapRect:r animated:YES];
    
//    MKAnnotationView *view = [MKAnnotationView new];
//    static NSString *annotationIdentifier = @"annotationIdentifier";
//    MKPinAnnotationView *pinView = [[MKPinAnnotationView alloc]  initWithAnnotation:annot reuseIdentifier:annotationIdentifier];
//    pinView.pinTintColor = [UIColor greenColor];
//    pinView.canShowCallout = YES;
//    pinView.image = [UIImage imageNamed:@"pin"];
//    view = pinView;
//    [self.mapKitView addSubview:view];
//    //        // You may need to resize the image here.
//    //        annotationView.image = flagImage;
    
}

- (void)getLocationDetails:(double)lattitude long:(double)logitude {
    
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:lattitude longitude:logitude];
    NSLog(@"GEO",ceo);
    
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) { CLPlacemark *placemark = [placemarks objectAtIndex:0]; if (placemark) { NSLog(@"placemark %@",placemark);
        NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
        NSLog(@"addressDictionary %@", placemark.addressDictionary);
        NSLog(@"placemark %@",placemark.region); NSLog(@"placemark %@",placemark.country);
        // Give Country Name
        NSLog(@"placemark %@",placemark.locality);
        // Extract the city name
        NSLog(@"location %@",placemark.name);
        NSLog(@"location %@",placemark.ocean);
        NSLog(@"location %@",placemark.postalCode);
        NSLog(@"location %@",placemark.subLocality);
        NSLog(@"location %@",placemark.location);
        NSLog(@"I am currently at %@",locatedAt);
        self->locationDetailedAddress = locatedAt;
        self->locationTitle = placemark.name;
    }
    else
    { NSLog(@"Could not locate"); } } ];
}

#pragma mark - UISearchBar Delegate Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText length] != 0) {
        [self searchlocation];
        self.tableView.hidden = NO;
    } else {
        
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    self.searchBar.text = @"";
    self.tableView.hidden = YES;
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

-(void)searchlocation{
    
    // Create a search request with a string
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:self.searchBar.text];
    
    // Create the local search to perform the search
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (!error) {
            self.geofences = [NSMutableArray new];
            for (MKMapItem *mapItem in [response mapItems]) {
                NSLog(@"Name: %@, Placemark title: %@", [mapItem name], [[mapItem placemark] title]);
                
                // Fetch Geofence
                CLRegion *region = mapItem.placemark.region;
                CLLocationCoordinate2D center = [region center];
                
                LocationStruct *objStruct = [[LocationStruct alloc] init];
                objStruct.latitude = center.latitude;
                objStruct.longitude = center.longitude;
                objStruct.locationDetailedAddress = [[mapItem placemark] title];
                objStruct.locationTitle = [mapItem name] ;
                [self.geofences addObject:objStruct];
            }
            
            [self.tableView reloadData];
            
        } else {
            NSLog(@"Search Request Error: %@", [error localizedDescription]);
        }
    }];
}

#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.geofences ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.geofences.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:GeofenceCellIdentifier];
    
    LocationStruct *locationObj = [self.geofences objectAtIndex:[indexPath row]];
    
    NSString *locationDetailedAddressText = locationObj.locationTitle;
    [cell.textLabel setText:locationDetailedAddressText];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.contentMode = NSLineBreakByWordWrapping;
    
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.contentMode = NSLineBreakByWordWrapping;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    [cell.detailTextLabel setText:locationObj.locationDetailedAddress];
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LocationStruct *locationObj = [self.geofences objectAtIndex:[indexPath row]];
    LocationStruct *locationObjStruct = [LocationStruct new];
    locationObjStruct.latitude = locationObj.latitude;
    locationObjStruct.longitude = locationObj.longitude;
    locationObjStruct.locationTitle = locationObj.locationTitle;
    locationObjStruct.locationDetailedAddress = locationObj.locationDetailedAddress;
    [self.delegate saveLocationDelegate:locationObjStruct];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
