//
//  FavouriteLocationsListViewController.h
//  Alerts Map App
//
//  Created by Manikantesh on 25/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapKitViewController.h"
#import "Structures.h"
#import "SettingsViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavouriteLocationsListViewController : UIViewController


@end

NS_ASSUME_NONNULL_END
