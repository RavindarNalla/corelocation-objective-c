//
//  FavouriteLocationsListViewController.m
//  Alerts Map App
//
//  Created by Manikantesh on 25/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import "FavouriteLocationsListViewController.h"

@import UserNotifications;

@interface FavouriteLocationsListViewController ()<CLLocationManagerDelegate,UNUserNotificationCenterDelegate,LocationDelegate,RadiusDistanceDelegate>{
    BOOL _didStartMonitoringRegion;
    NSString *locatedAt;
    int minimumDistance;
    double currentLatitude;
    double currentLongitude;
}

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *geofences;

@property NSUserDefaults *userDefaults;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FavouriteLocationsListViewController

static NSString *GeofenceCellIdentifier = @"GeofenceCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.geofences = [NSMutableArray new];
    
    [self setupView];
    [self initCoreLocation];
    [self initNotification];
    [self updateView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Update Helper
    _didStartMonitoringRegion = YES;
    
    // Start Updating Location
    [self.locationManager startUpdatingLocation];
    [[self.locationManager monitoredRegions] allObjects];
}

- (void)setupView {
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    // Create Add Button
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewLocation:)];
    
    // Create Edit Button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", nil) style:UIBarButtonItemStylePlain target:self action:@selector(editTableView:)];
    
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    if ([self.userDefaults objectForKey:@"Geofences"]){
        self.geofences = [NSMutableArray new];
        self.geofences = [self.userDefaults objectForKey:@"Geofences"];
    }
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedArray = [currentDefaults objectForKey:@"Geofences"];
    if (savedArray != nil)
    {
        NSArray *oldArray = [NSKeyedUnarchiver unarchiveObjectWithData:savedArray];
        if (oldArray != nil) {
            self.geofences = [[NSMutableArray alloc] initWithArray:oldArray];
        } else {
            self.geofences = [[NSMutableArray alloc] init];
        }
    }
    
    self.title = @"Favourite Locations";
    
    NSNumber *savedDistance = [[NSUserDefaults standardUserDefaults] objectForKey:@"DistanceInMeters"];
    int distanceInMetres = savedDistance.intValue;
    if(distanceInMetres){
        minimumDistance = distanceInMetres;
    }else{
        int distanceInMetres = 1 * 1000;
        NSNumber *distanceNumber = [NSNumber numberWithInt: distanceInMetres];
        [[NSUserDefaults standardUserDefaults] setObject: distanceNumber forKey:@"DistanceInMeters"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        minimumDistance = 1000;
    }
    
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingsButton.frame = CGRectMake(self.view.frame.size.width - 60, self.view.frame.size.height - 70, 40, 40);
    [settingsButton addTarget:self action:@selector(settingsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingsButton.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"settings.jpg"] forState:UIControlStateNormal];
    settingsButton.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:settingsButton];
    
}

- (void)addNewLocation:(id)sender {
    
    MapKitViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MapKitViewController"];
    vc.delegate = self;
    if (self.geofences.count > 0){
        vc.typeStr = @"OLD";
    }else{
        vc.typeStr = @"NEW";
    }
    [self.navigationController pushViewController:vc animated:NO];
}

- (void)settingsClicked:(id)sender {
    
    SettingsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:NO];
}


- (void)initCoreLocation {
    
    // Configure Location Manager
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // To save the battery life
    self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    //[self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    
    /* Notify changes when device has moved x meters.
     * Default value is kCLDistanceFilterNone: all movements are reported.
     */
    self.locationManager.distanceFilter = 30.0f;
    
    /* Notify heading changes when heading is > 5.
     * Default value is kCLHeadingFilterNone: all movements are reported.
     */
    self.locationManager.headingFilter = 15;
    
}


- (void)initNotification {
    
    // Request Notification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert)
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!error) {
                                  NSLog(@"request authorization succeeded!");
                              }
                          }];
    
    [center requestAuthorizationWithOptions:options
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!granted) {
                                  NSLog(@"Something went wrong");
                              }
                          }];
    
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus != UNAuthorizationStatusAuthorized) {
            // Notifications not allowed
        }
    }];
    
}

- (void)updateView {
    
    if ([self.geofences count]) {
        // Update Table View
        [self.tableView setEditing:NO animated:YES];
        
        // Update Edit Button
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
        [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Edit", nil)];
        
    } else {
        // Update Edit Button
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Edit", nil)];
    }
    
}

- (void)editTableView:(id)sender {
    // Update Table View
    [self.tableView setEditing:![self.tableView isEditing] animated:YES];
    
    // Update Edit Button
    if ([self.tableView isEditing]) {
        [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Done", nil)];
    } else {
        [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Edit", nil)];
    }
}

- (void)setUpGeofences {
    
    if (self.geofences.count > 0){
        
        LocationStruct *locationObj = self.geofences[0];
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(locationObj.latitude,
                                                                   locationObj.longitude);
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:center
                                                                     radius:minimumDistance
                                                                 identifier:@"Favourite Location"];
        [self.locationManager startMonitoringForRegion:region];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        self.locationManager.allowsBackgroundLocationUpdates = YES;
        [self sort_distance];
    }
    
}

-(void)sort_distance{
    CLLocationDegrees firstLat = currentLatitude ;
    CLLocationDegrees firstLong = currentLongitude ;
    
    CLLocation *userLocation = [[CLLocation alloc]
                                initWithLatitude:firstLat
                                longitude:firstLong];
    
    [_geofences sortUsingComparator:^NSComparisonResult(LocationStruct *position1, LocationStruct *position2) {
        
        CLLocationDistance distanceWithPosition1 = [self distanceOfCurrentPosition:userLocation WithPosition:position1];
        CLLocationDistance distanceWithPosition2 = [self distanceOfCurrentPosition:userLocation WithPosition:position2];
        
        if (distanceWithPosition1 > distanceWithPosition2) {
            return NSOrderedDescending;
        }else if(distanceWithPosition2 > distanceWithPosition1){
            return NSOrderedAscending;
        }else{
            return NSOrderedSame;
        }
        
    }];
    
    NSLog(@"Order :%@",_geofences);
    
    if (_geofences.count > 0) {
        
        //        for (LocationStruct *structLoc in _geofences) {
        //            //NSLog(@"-----%@",structLoc.locationDetailedAddress);
        //        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self setUpGeofences];
        });
    }
    
}

-(CLLocationDistance )distanceOfCurrentPosition:(CLLocation *)userLocation WithPosition:(LocationStruct *)postition{
    
    CLLocationDegrees secondLat = postition.latitude;
    CLLocationDegrees secondLong = postition.longitude;
    
    CLLocation *pinLocation = [[CLLocation alloc]
                               initWithLatitude:secondLat
                               longitude:secondLong];
    
    CLLocationDistance distance = [pinLocation distanceFromLocation:userLocation]/1000;
    CLLocationDistance kilometers = distance /1000;
    return kilometers;
}

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    NSLog(@"NEW LOCATION");
    //    // Stop location updates when they aren't needed anymore
    //    [self.locationManager stopUpdatingLocation];
    //
    //    // Disable background location updates when they aren't needed anymore
    //    self.locationManager.allowsBackgroundLocationUpdates = NO;
    
    if (locations && [locations count] && _didStartMonitoringRegion) {
        // Update Helper
        _didStartMonitoringRegion = YES;
        NSLog(@"Updating:......");
        // Fetch Current Location
        CLLocation *location = [locations objectAtIndex:0];
        currentLatitude = location.coordinate.latitude ;
        currentLongitude = location.coordinate.longitude ;
        
        // Initialize Region to Monitor
        CLRegion *region = [[CLRegion alloc] initCircularRegionWithCenter:[location coordinate] radius:250.0 identifier:[[NSUUID UUID] UUIDString]];
        
        // Start Monitoring Region
        [self.locationManager startMonitoringForRegion:region];
        [self.locationManager stopUpdatingLocation];
        
        [self sort_distance];
    }
    
    
}

- (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestAlwaysAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            [self sort_distance];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            [self sort_distance];
            break;
        case kCLAuthorizationStatusRestricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break;
        case kCLAuthorizationStatusDenied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region {
    NSLog(@"didEnter : %@", region);
    CLLocationCoordinate2D center = [region center];
    NSString *locationDetailedAddress =  [self getLocationDetails:center.latitude long:center.longitude];
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:center.latitude longitude:center.longitude];
    
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) { CLPlacemark *placemark = [placemarks objectAtIndex:0]; if (placemark) {
        locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
        NSLog(@"I am currently at %@",locatedAt);
        if (self.geofences.count > 0){
            dispatch_async(dispatch_get_main_queue(), ^{
                UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
                content.title = [NSString localizedUserNotificationStringForKey:@"Alert! You are in your favourite location region.." arguments:nil];
                content.body = [NSString localizedUserNotificationStringForKey:locatedAt
                                                                     arguments:nil];
                content.sound = [UNNotificationSound defaultSound];
                
                // Update application icon badge number
                content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
                
                // Deliver the notification in three seconds.
                UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                              triggerWithTimeInterval:3 repeats:NO];
                UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"ThreeSeconds"
                                                                                      content:content trigger:trigger];
                
                // Schedule the notification.
                UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
                center.delegate = self;
                [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                    if (!error) {
                        NSLog(@"add NotificationRequest succeeded!");
                    }
                }];
            });
        }
    }else
    { NSLog(@"Could not locate");
    }
    }];
    
    [self sort_distance];
    
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    CLLocationCoordinate2D center = [region center];
    NSLog(@"didExitRegion : %@", region);
    //NSString *locationDetailedAddress =  [self getLocationDetails:center.latitude long:center.longitude];
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:center.latitude longitude:center.longitude];
    NSLog(@"GEO",ceo);
    
    NSString *locatedAtTemp = @"";
    
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) { CLPlacemark *placemark = [placemarks objectAtIndex:0]; if (placemark) {
        locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
        NSLog(@"I am currently at %@",locatedAt);
        if (self.geofences.count > 0){
            dispatch_async(dispatch_get_main_queue(), ^{
                UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
                content.title = [NSString localizedUserNotificationStringForKey:@"Alert! You had exited your favourite location region.." arguments:nil];
                content.body = [NSString localizedUserNotificationStringForKey:locatedAt
                                                                     arguments:nil];
                content.sound = [UNNotificationSound defaultSound];
                
                // Update application icon badge number
                content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
                
                // Deliver the notification in three seconds.
                UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                              triggerWithTimeInterval:3 repeats:NO];
                UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"ThreeSeconds"
                                                                                      content:content trigger:trigger];
                
                // Schedule the notification.
                UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
                center.delegate = self;
                [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                    if (!error) {
                        NSLog(@"add NotificationRequest succeeded!");
                    }
                }];
            });
        }
    }else
     { NSLog(@"Could not locate");
     }
    }];
    [self sort_distance];
}
     
     - (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
         NSLog(@"Error: %@", error);
     }
     
     - (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
         NSLog(@"Start monitoring for region: %@", region.identifier);
         
         CLLocationCoordinate2D center = [region center];
         NSLog(@"Start monitoring for latitude: %f",center.latitude);
         NSLog(@"Start monitoring for longitude: %f",center.longitude);
         [self.locationManager requestStateForRegion:region];
     }
     
     - (void)locationManager:(CLLocationManager *)manager
              didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
                  // When regions are initialized, see if we are already within the geofence.
                  switch (state) {
                      case CLRegionStateInside:{
                          NSLog(@"CLRegionStateInside : %@", region);
                          break;
                      }
                      case CLRegionStateUnknown: NSLog(@"Unknown");
                      case CLRegionStateOutside: NSLog(@"Outside");
                      default: break;
                  }
              }
     
#pragma mark - UITableView Delegate & DataSource Methods
     
     - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
         return self.geofences ? 1 : 0;
     }
     
     - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
         return (self.geofences.count > 0) ? self.geofences.count : 1;
     }
     
     - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
         
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:GeofenceCellIdentifier];
         
         if (self.geofences.count > 0){
             LocationStruct *locationObj = [self.geofences objectAtIndex:[indexPath row]];
             
             NSString *locationDetailedAddressText = locationObj.locationDetailedAddress;
             [cell.textLabel setText:locationDetailedAddressText];
             cell.textLabel.numberOfLines = 0;
             cell.textLabel.contentMode = NSLineBreakByWordWrapping;
             
             cell.detailTextLabel.numberOfLines = 0;
             cell.detailTextLabel.contentMode = NSLineBreakByWordWrapping;
             cell.textLabel.textAlignment = NSTextAlignmentLeft;
             [cell.detailTextLabel setText:[NSString stringWithFormat:@"Latitude: %.5f | Longitude: %.5f", locationObj.latitude, locationObj.longitude]];
         }else{
             
             [cell.textLabel setText:@"No Location Available"];
             cell.detailTextLabel.numberOfLines = 0;
             cell.detailTextLabel.contentMode = NSLineBreakByWordWrapping;
             cell.textLabel.textAlignment = NSTextAlignmentCenter;
             
             [cell.detailTextLabel setText:[NSString stringWithFormat:@"Please add a location"]];
         }
         
         
         return cell;
     }
     - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
     {
         self.tableView.estimatedRowHeight = 50.0f;
         return UITableViewAutomaticDimension;
     }
     
     - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
         
     }
     
     - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
         return YES;
     }
     
     - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
         return NO;
     }
     
     - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
         if (editingStyle == UITableViewCellEditingStyleDelete) {
             
             LocationStruct *locationObj = [self.geofences objectAtIndex:[indexPath row]];
             [self.geofences removeObject:locationObj];
             [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self.geofences] forKey:@"Geofences"];
             [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
             
             // Update View
             [self updateView];
         }
     }
     
     
     
#pragma mark - UNUserNotificationCenterDelegate Delegate Methods
     
     -(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
         
         completionHandler(UNNotificationPresentationOptionAlert + UNNotificationPresentationOptionSound + UNNotificationPresentationOptionBadge);
     }
     
     - (void)displayNotificationForUser:(NSString *)title withBody:(NSString *)body {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
             content.title = [NSString localizedUserNotificationStringForKey:title arguments:nil];
             content.body = [NSString localizedUserNotificationStringForKey:body
                                                                  arguments:nil];
             content.sound = [UNNotificationSound defaultSound];
             
             // Update application icon badge number
             content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
             
             // Deliver the notification in three seconds.
             UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                           triggerWithTimeInterval:3 repeats:NO];
             UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"ThreeSeconds"
                                                                                   content:content trigger:trigger];
             
             // Schedule the notification.
             UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
             center.delegate = self;
             [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                 if (!error) {
                     NSLog(@"add NotificationRequest succeeded!");
                 }
             }];
         });
     }
     
     
     -(void)saveLocationDelegate:(LocationStruct *)locStruct{
         
         [self.geofences addObject:locStruct];
         [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self.geofences] forKey:@"Geofences"];
         [self updateView];
         [self.tableView reloadData];
     }
     
     -(void)saveDistanceDelegate:(int)distance{
         
         int distanceInMetres = distance * 1000;
         NSNumber *distanceNumber = [NSNumber numberWithInt: distanceInMetres];
         [[NSUserDefaults standardUserDefaults] setObject: distanceNumber forKey:@"DistanceInMeters"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         minimumDistance = distanceInMetres;
     }
     
     
     - (NSString *)getLocationDetails:(double)lattitude long:(double)logitude {
         
         CLGeocoder *ceo = [[CLGeocoder alloc]init];
         CLLocation *loc = [[CLLocation alloc]initWithLatitude:lattitude longitude:logitude];
         NSLog(@"GEO",ceo);
         
         NSString *locatedAtTemp = @"";
         
         [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) { CLPlacemark *placemark = [placemarks objectAtIndex:0]; if (placemark) {
             self->locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSLog(@"I am currently at %@",self->locatedAt);
         }
         else
         { NSLog(@"Could not locate");
         }
             
         }];
         return  locatedAt;
     }
     
     
     @end
