//
//  MapKitViewController.h
//  Alerts Map App
//
//  Created by Manikantesh on 27/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Structures.h"

NS_ASSUME_NONNULL_BEGIN

@protocol LocationDelegate <NSObject>

@optional
-(void)saveLocationDelegate:(LocationStruct *)locStruct ;
@end

@interface MapKitViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapKitView;
@property(nonatomic,strong)id<LocationDelegate>delegate;
@property (nonatomic,strong) NSString *typeStr;

@end

NS_ASSUME_NONNULL_END
