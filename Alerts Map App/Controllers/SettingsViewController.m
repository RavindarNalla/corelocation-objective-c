//
//  SettingsViewController.m
//  Alerts Map App
//
//  Created by Manikantesh on 28/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupView];
}

- (void)setupView {
    
    self.title = @"Settings";
    
    // Create SAVE Button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveDistance:)];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Save", nil)];
    
    NSNumber *savedDistance = [[NSUserDefaults standardUserDefaults] objectForKey:@"DistanceInMeters"];
    int distanceInMetres = savedDistance.intValue;
    if(distanceInMetres){
        self.distanceTF.text = [NSString stringWithFormat:@"%d",(distanceInMetres/1000)];
    }else{
        self.distanceTF.text = @"";
    }
}

- (void)saveDistance:(id)sender {
    
    if (![self.distanceTF.text isEqualToString:@""]){
        [self.delegate saveDistanceDelegate:[self.distanceTF.text intValue]];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        
    }
    
}

@end
