//
//  SettingsViewController.h
//  Alerts Map App
//
//  Created by Manikantesh on 28/05/19.
//  Copyright © 2019 Manikantesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RadiusDistanceDelegate <NSObject>

@optional
-(void)saveDistanceDelegate:(int)distance ;
@end

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *distanceTF;

@property(nonatomic,strong)id<RadiusDistanceDelegate>delegate;
@end

NS_ASSUME_NONNULL_END
